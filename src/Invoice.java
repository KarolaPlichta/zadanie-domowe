import java.util.ArrayList;
import java.util.Date;


public class Invoice {
	private ArrayList<Order> ordersList;
	private Date date;
	private Date paidOffDay;
	private int daysOffPaid;
	private String invoiceNumber;
	
	public Invoice(String invoiceNumber){
		this.invoiceNumber = invoiceNumber;
		this.ordersList = new ArrayList<Order>();
	}
	
	public void add(Order s){
		this.ordersList.add(s);
	}
	
	public Order getOrder(Order name){
		Order result = null;
		for(Order current : this.ordersList){
			if (current.copy() == name) {
				result = current.copy();
				break;
			}
		}
		return result;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getPaidOffDay() {
		return paidOffDay;
	}

	public void setPaidOffDay(Date paidOffDay) {
		this.paidOffDay = paidOffDay;
	}

	public int getDaysOffPaid() {
		return daysOffPaid;
	}

	public void setDaysOffPaid(int daysOffPaid) {
		this.daysOffPaid = daysOffPaid;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public double getInvoiceNettoPrice(){
		double nettoPrice = 0;
		for (Order current : this.ordersList){
			nettoPrice+=current.getOrderNettoPrice();
		}
		return nettoPrice;
	}
	
	public double getInvoiceBruttoPrice(){
		double bruttoPrice = 0;
		for(Order current : this.ordersList){
			bruttoPrice+=current.getOrderBruttoPrice();
		}
		return bruttoPrice;
	}


	

}
