public class Order {
	
	
	private double priceNetto;
	private int count;
	private int tax;
	private Product product = new Product();
	
	
	public double getPriceNetto() {
		return priceNetto;
	}

	public void setPriceNetto(double priceNetto) {
		this.priceNetto = priceNetto;
	}

	public double getPriceBrutto() {
		return this.getPriceNetto() * this.getTax()+ this.getPriceNetto();
	}

	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}
	public double getOrderNettoPrice(){
		return getCount()*getPriceNetto();
	}
	public double getOrderBruttoPrice(){
		return getCount()*getPriceBrutto();
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Order copy(){
		Order order = new Order();
		order.setProduct(this.getProduct());
		order.setCount(this.getCount());
		order.setTax(this.getTax());
		order.setPriceNetto(this.getPriceNetto());
		return order;
	}

	

}