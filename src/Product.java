
public class Product {
	private String name;
	private String unit;
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name= name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public Product copy(){
		Product product = new Product();
		product.setName(this.getName());
		product.setUnit(this.getUnit());
		return product;
	}

}
